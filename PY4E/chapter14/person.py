from datetime import datetime, timedelta

class Person:
    
    def __init__(self, fullname, birthdate, address, gender):
        self.fullname = fullname
        self.birthdate = birthdate
        self.address = address
        self.gender = gender

    def get_firstname(self):
        return str(self.fullname.split(' ')[0])

    def get_lastname(self):
        return str(self.fullname.split(' ')[1])

    def get_age(self):
        self.birthdate = datetime.strptime(self.birthdate, "%d-%m-%Y")
        age = abs((datetime.today() - self.birthdate).days)//365
        return int(age)

    def get_address(self):
        return list(self.address[0:3])

    def get_full_info(self):
        return {
            "First name":self.get_firstname(),
            "Last name":self.get_lastname(),
            "Age":self.get_age(),
            "Address":self.get_address()
            }

class Extended_Person(Person):

    def __init__(self, fullname, birthdate, address, gender, pet_type, pet_name, monthly_income):
        super().__init__(fullname, birthdate, address, gender)
        self.pet_type = pet_type
        self.pet_name = pet_name
        self.monthly_income = monthly_income

    def get_pet(self):
        return str(self.pet_type + ' ' + self.pet_name)

    def get_income(self):
        self.monthly_income = int(self.monthly_income)
        if self.monthly_income > 10000:
            income = "low"
        elif 10000 <= self.monthly_income <= 20000:
            income = "middle"
        else:
            income = "high"
        return str(income)


#person1 = Person("John Smith","24-12-1999",["Seebladsgade", "1", "Odense C","5000","DK"], "Male")
#person2 = Person("William Johnson","31-12-1929",["Teststreet", "99", "Aalborg","9000","DK"], "Male")
#person3 = Person("Jessica Parker","01-01-1948",["Street", "42", "London","E1 6AN","UK"], "Female")

#person4 = Extended_Person("Peter Parker","01-01-1900",["Spiderman street","99","Spidertown","9999","US"],"Male","Dog","Benny","1000")
#person5 = Extended_Person("Bruce Wayne","01-01-1920",["Batman street","99","Battown","9999","US"],"Male","Cat","Lars","15000")
#person6 = Extended_Person("Robert Downey Jr.","01-01-1940",["Ironman street","99","Irontown","9999","US"],"Male","Dragon","Bent","99999")

#persons = [person1,person2,person3]

#extended_persons = [person4,person5,person6]

#for person in persons:
#    print(person.get_full_info())
    
#for ext_person in extended_persons:
#    print(ext_person.get_pet())
#    print(ext_person.get_income())

def main():

    print(Extended_Person("Peter Parker","01-01-1900",["Spiderman street","99","Spidertown","9999","US"],"Male","Dog","Benny","1000").get_full_info())

if __name__ == '__main__':
    main()
