from person import Extended_Person

person1 = Extended_Person("Peter Parker","01-01-1900",["Spiderman street","99","Spidertown","9999","US"],"Male","Dog","Benny","1000")
person2 = Extended_Person("Bruce Wayne","01-01-1920",["Batman street","99","Battown","9999","US"],"Male","Cat","Lars","15000")
person3 = Extended_Person("Robert Downey Jr.","01-01-1940",["Ironman street","99","Irontown","9999","US"],"Male","Dragon","Bent","99999")

persons = [person1,person2,person3]
    
for person in persons:
    print(person.get_full_info(), "\n",
        person.get_pet(), "\n",
        person.get_income()
    )