def userInput():
    global inputMaxValue
    global inputMinValue
    inputMaxValue = 0
    inputMinValue = 0
    loopActive = True
    print('To see the max and minimum all the values entered, type 'done'!')
    while loopActive:
        try:
            inputValue = input('Enter a number: ')
            if inputValue == 'done':
                loopActive = False
                outputData()
            else:
                inputValue = float(inputValue)
                if inputMaxValue == 0 and inputMinValue == 0:
                    inputMaxValue = inputValue
                    inputMinValue = inputValue
                if inputValue > inputMaxValue:
                    inputMaxValue = inputValue
                if inputValue < inputMinValue:
                    inputMinValue = inputValue
        except ValueError:
            print('Invalid input!')

def outputData():
    print('\n' + 'Max value is: ' + str(inputMaxValue) + '\n' + 'Minimim value is: ' + str(inputMinValue) + '\n')

userInput()

