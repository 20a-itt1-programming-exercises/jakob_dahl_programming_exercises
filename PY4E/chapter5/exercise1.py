def userInput():
    global inputTotalValue
    global inputTotalCount
    inputTotalCount = 0
    inputTotalValue = 0
    loopActive = True
    print('To see the total, count and average for all the values entered, type 'done'!')
    while loopActive:
        try:
            inputValue = input('Enter a number: ')
            if inputValue == 'done':
                loopActive = False
                outputData()
            else:
                inputTotalValue = inputTotalValue + int(inputValue)
                inputTotalCount = inputTotalCount + 1
        except ValueError:
            print('Invalid input!')

def outputData():
    print('\n' + 'Total: ' + str(inputTotalValue) + '\n' + 'Count: ' + str(inputTotalCount) + '\n' + 'Average: ' + str(inputTotalValue / inputTotalCount) + '\n')

userInput()