def WorkHoursInput():
    global hours
    try: 
        hours = input('Please enter your work hours: ')
        hours = float(hours)
    except ValueError:
        print(''' + hours + ''' + ' is not a valid float!')
        WorkHoursInput()

def RateInput():
    global rate
    try: 
        rate = input('Please enter your rate per hour: ')
        rate = float(rate)
    except ValueError:
        print(''' + rate + ''' + ' is not a valid float!')
        RateInput()

def CalculatePay():
    WorkHoursInput()
    RateInput()
    if hours < 40:
        pay = hours * rate
        print('\n' + 'Your normal pay is: ' + str(pay))
    else:
        normalPay = (hours - (hours - 40)) * rate
        print('\n' + 'Your normal pay is: ' + str(normalPay))
        overtimePay = (hours - 40) * (rate * 1.5)
        print('Your overtime pay is: ' + str(overtimePay))
        pay = normalPay + overtimePay
    print('\n' + 'Your total pay for ' + str(int(hours)) + ' work hours is: ' + str(int(pay)))

CalculatePay()