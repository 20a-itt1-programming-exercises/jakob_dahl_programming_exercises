hours = float(input('Please enter your work hours: '))
rate = float(input('Please enter your rate per hour: '))
if hours < 40:
    pay = hours * rate
else:
    normalPay = (hours - (hours - 40)) * rate
    overtimePay = (hours - 40) * (rate * 1.5)
    pay = normalPay + overtimePay
print('Your pay for: ' + str(int(hours)) + ' work hours is: ' + str(int(pay)))