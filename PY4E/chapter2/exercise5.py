celsiusIsInteger = True
while celsiusIsInteger:
	try:
		celsius = input('Enter a celsius temperature value: ')
		int(celsius)
		celsiusIsInteger = True
		break
	except ValueError:
		print(''' + str(celsius) + ''' + ' is not an integer, please enter an integer value!')
		if celsiusIsInteger:
			continue
celsiusToFahrenheit = (int(celsius) * 9/5) + 32
print('The Celsius value: ' + str(celsius) + ' converted to Fahrenheit is: ' + str(celsiusToFahrenheit))
