'''
Exercise 3: Write a program that reads a file and prints the letters
in decreasing order of frequency.
Your program should convert all the input to lower case and only count the letters a-z.
Your program should not count spaces, digits, punctuation, or anything other than the letters a-z.
Find text samples from several different languages and see how letter frequency varies between languages.
Compare your results with the tables at https://wikipedia.org/wiki/Letter_frequencies.
'''

illegalCharacters = ['\n',' ','-','.',',','1','2','3','4','5','6','7','8','9','0','!','\\','*','_','{','}','[',']','(',')','>','#','+','$','\'']

txtfile = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter10/danish.txt')

letterHistogram = dict()

for line in txtfile:
    if line != '':
        for char in illegalCharacters:
            line = line.replace(char,"")
        line = line.lower()

        for letter in line:
            if letter not in letterHistogram:
                letterHistogram[letter] = 1
            else:
                letterHistogram[letter] += 1
        else:
            continue

txtfile.close()
lst = list()

for letter, count in list(letterHistogram.items()):
    lst.append((count, letter))

lst.sort()




