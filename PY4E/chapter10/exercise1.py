'''
Exercise 1: Revise a previous program as follows:
Read and parse the “From” lines and pull out the addresses from the line.
Count the number of messages from each person using a dictionary.

After all the data has been read, print the person with the most commits
by creating a list of (count, email) tuples from the dictionary.
Then sort the list in reverse order and print out the person who has the most
commits.
'''

try:
    mails_file = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter10/mbox.txt')
    mails_histogram = dict()
    largest = None
    max_receiver = ''

    for line in mails_file:
        if line.startswith('From: '):
            address = line[line.find(' ') + 1:].rstrip()
            if address not in mails_histogram:
                mails_histogram[address] = 1
            else:
                mails_histogram[address] += 1

    mails_file.close()

    lst = list()

    for name, count in list(mails_histogram.items()):
        lst.append((name, count))

    lst.sort(reverse=True)

    print(*lst[0])

except FileNotFoundError:
    print('File not found')
