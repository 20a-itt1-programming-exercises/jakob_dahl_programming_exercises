'''
Change your socket program so that it counts the number
of characters it has received and stops displaying any text after it has
shown 3000 characters. The program should retrieve the entire docu-
ment and count the total number of characters and display the count
of the number of characters at the end of the document.
'''

import socket
import time

count = 0

websiteURL = "http://data.pr4e.org/mbox.txt"

try:
    websiteHostname = websiteURL.split('/')[2]

    mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mysock.connect((websiteHostname, 80))
    cmd = f"GET {websiteURL} HTTP/1.0\r\n\r\n".encode()
    mysock.send(cmd)

    while True:
        data = mysock.recv(3000)
        time.sleep(0.005)
        if len(data) < 1: break
        count = count + len(data)
        print(data.decode(),end='')
        print(f"\n\n ---- Length of data: {len(data)}, Total length of data: {count} ----\n")

    mysock.close()
except Exception as e:
    print(f"Error occured: {e}")