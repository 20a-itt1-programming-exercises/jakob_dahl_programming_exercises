import urllib.request
import time

# http://data.pr4e.org/mbox.txt

websiteURL = input("Enter a valid url:\n> ")
size = 0

try:
    fhand = urllib.request.urlopen(websiteURL)
    while True:
        data = fhand.read(3000)
        time.sleep(0.005)
        if len(data) < 1: break
        size = size + len(data)
        print(data.decode())
        print(f"\n\n ---- Length of data: {len(data)}, Total length of data: {size} ----\n")
except Exception as e:
    print(f"Error occured: {e}")