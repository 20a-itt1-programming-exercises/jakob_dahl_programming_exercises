import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

# https://docs.python.org
# https://www.dr.dk/nyheder/seneste/spacex-rumfartoej-er-eksploderet

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

try:
	url = input('Enter a url\n> ')
	html = urllib.request.urlopen(url, context=ctx).read()
	soup = BeautifulSoup(html, 'html.parser')

	# Retrieve all of the anchor tags
	tags = soup('p')
	count = len(tags)
except Exception as e:
	print(e)

print(f"Found a total of: {count} 'p' tags")