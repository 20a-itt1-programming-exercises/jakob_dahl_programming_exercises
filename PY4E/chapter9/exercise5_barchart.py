'''
Exercise 5: This program records the domain name (instead of the
address) where the message was sent from instead of who the mail came
from (i.e., the whole email address). At the end of the program, print
out the contents of your dictionary.
'''

import matplotlib.pyplot as plt

mailsHistogram = dict()

fhand = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter9/mbox.txt')

for line in fhand:
    if line.startswith('From: '):
        domain = line[line.find('@'):]
        if domain not in mailsHistogram:
            mailsHistogram[domain] = 1
        else:
            mailsHistogram[domain] += 1

fhand.close()

print(sorted(mailsHistogram))

sortList = list()
for sender,count in list(mailsHistogram.items()):
    sortList.append((count,sender))
sortList.sort()

senderList = list()
countList = list()

for count, sender in sortList:
    senderList.append(sender)
    countList.append(count)

plt.style.use('seaborn')
plt.barh(senderList,countList,color='#444444', height=0.75, label='mails/day')
plt.ylabel('Sender')
plt.xlabel('Mails')
plt.title('Mails a day chart')
plt.legend()
plt.tight_layout()
plt.legend(bbox_to_anchor=(0.95,0.2),bbox_transform=plt.gcf().transFigure)
plt.show()