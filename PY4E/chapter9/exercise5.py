'''
Exercise 5: This program records the domain name (instead of the
address) where the message was sent from instead of who the mail came
from (i.e., the whole email address). At the end of the program, print
out the contents of your dictionary.
'''

mailsHistogram = dict()

fhand = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter9/mbox.txt')

for line in fhand:
    if line.startswith('From: '):
        domain = line[line.find('@'):]
        if domain not in mailsHistogram:
            mailsHistogram[domain] = 1
        else:
            mailsHistogram[domain] += 1

fhand.close()

print(sorted(mailsHistogram))

