'''
Exercise 1: Download a copy of the file www.py4e.com/code3/words.txt
Write a program that reads the words in words.txt and stores them as
keys in a dictionary. It doesn’t matter what the values are. Then you
can use the in operator as a fast way to check whether a string is in the
dictionary.
'''

wordsDict = dict()
keyIdx = 0

fhand = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter9/words.txt')
for line in fhand:
    words = line.split()
    for word in words:
        wordsDict[word] = [keyIdx]
        keyIdx += 1

print('problem' in wordsDict)
