'''
Exercise 2: Write a program that categorizes each mail message by
which day of the week the commit was done. To do this look for lines
that start with “From”, then look for the third word and keep a running
count of each of the days of the week. At the end of the program print
out the contents of your dictionary (order does not matter).
'''

mailsDict = dict()

fhand = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter9/mbox-short.txt')
for line in fhand:
    if line.startswith('From '):
        linewords = line.split()
        if linewords[2] not in mailsDict:
            mailsDict[linewords[2]] = 1
        else:
            mailsDict[linewords[2]] = mailsDict[linewords[2]] + 1

print(mailsDict)
