'''
Exercise 3: Write a program to read through a mail log, build a his-
togram using a dictionary to count how many messages have come from
each email address, and print the dictionary.
'''

mailsHistogram = dict()

fhand = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter9/mbox-short.txt')
for line in fhand:
    if line.startswith('From '):
        linewords = line.split()
        if linewords[1] not in mailsHistogram:
            mailsHistogram[linewords[1]] = 1
        else:
            mailsHistogram[linewords[1]] = mailsHistogram[linewords[1]] + 1

print(mailsHistogram)
