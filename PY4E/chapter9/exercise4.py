'''
Exercise 4: Add code to the above program to figure out who has the
most messages in the file. After all the data has been read and the dic-
tionary has been created, look through the dictionary using a maximum
loop (see Chapter 5: Maximum and minimum loops) to find who has
the most messages and print how many messages the person has.
'''

mailsHistogram = dict()

fhand = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter9/mbox.txt')
messagesSent = None
maxSender = ''

for line in fhand:
    if line.startswith('From: '):
        mail = line[line.find(' ') + 1:].rstrip()
        if mail not in mailsHistogram:
            mailsHistogram[mail] = 1
        else:
            mailsHistogram[mail] += 1

fhand.close()

for name, i in mailsHistogram.items():
    if messagesSent is None or i > messagesSent:
        messagesSent = i
        maxSender = name

print(maxSender,messagesSent)

