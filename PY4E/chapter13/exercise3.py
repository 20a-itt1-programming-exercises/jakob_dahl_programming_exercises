import feedparser
import arrow

rssfeedurl = "https://gitlab.com/20a-itt1-programming-exercises.atom?feed_token=59uyUc9xs5zZanzjRwYT"
rssfeedData = feedparser.parse(rssfeedurl)

for idx in range(5):
    print(rssfeedData.entries[idx].author)
    print(arrow.get(rssfeedData.entries[idx].updated).to('local').format()[:-6])
    print(rssfeedData.entries[idx].summary_detail.value.split('<')[10][13:])
    print("\n")