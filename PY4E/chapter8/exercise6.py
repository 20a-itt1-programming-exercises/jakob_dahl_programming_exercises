'''
Rewrite the program that prompts the user for a list of
numbers and prints out the maximum and minimum of the numbers at
the end when the user enters “done”. Write the program to store the
numbers the user enters in a list and use the max() and min() functions to
compute the maximum and minimum numbers after the loop completes.
'''

numList = []

print('\nEnter a number or type 'done' to exit the program!')

while True:
    userInp = input('> ')

    if userInp == 'done':
        print(f'\nCount: {len(numList)}\nTotal: {sum(numList)}\nMax: {max(numList)}\nMin: {min(numList)}\n')
        print('Goodbye!')
        break
    try:
        userInp = float(userInp)
        numList.append(userInp)
    except ValueError:
        print(f'{userInp} is not a valid input!')
