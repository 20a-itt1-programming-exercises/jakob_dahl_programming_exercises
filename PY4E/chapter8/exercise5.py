'''
Write a program to read through the mail box data and
when you find line that starts with “From”, you will split the line into
words using the split function. We are interested in who sent the
message, which is the second word on the From line.
'''

fhand = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter7/mbox-short.txt')
counter = 0

for line in fhand:
    if line.startswith('From '):
        line_split = line.split()
        print(line_split[1])
    else:
        continue
