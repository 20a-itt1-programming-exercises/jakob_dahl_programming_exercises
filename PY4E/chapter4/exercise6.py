def computePay(hours,rate):
    if hours < 40:
        pay = hours * rate
        print('\n' + 'Your normal pay is: ' + str(pay))
    else:
        normalPay = (hours - (hours - 40)) * rate
        print('\n' + 'Your normal pay is: ' + str(normalPay))
        overtimePay = (hours - 40) * (rate * 1.5)
        print('Your overtime pay is: ' + str(overtimePay))
        pay = normalPay + overtimePay
    print('\n' + 'Your total pay for ' + str(int(hours)) + ' work hours is: ' + str(int(pay)))
        

def payInputs():
    try: 
        pIhours = input('Please enter your work hours: ')
        pIhours = float(pIhours)
    except ValueError:
        print(''' + hours + ''' + ' is not a valid float!')
        WorkHoursInput()
    try: 
        pIrate = input('Please enter your rate per hour: ')
        pIrate = float(pIrate)
    except ValueError:
        print(''' + rate + ''' + ' is not a valid float!')
    computePay(pIhours,pIrate)

payInputs()