def Score():
    try:
        score = input('Enter your score which is a value between 0.0 - 1.0: ')
        score = float(score)
        if score > 0.0 and score < 1.0:
            computeGrade
            (score)
        else:
            print(str(score) + ' is not a valid score!')
            Score()
    except ValueError:
        print(''' + score + ''' + ' is not a valid float!')
        Score()

def computeGrade(score):
    print('\n')
    if score >= 0.9:
        print('Your grade is: A')
    elif score >= 0.8:
        print('Your grade is: B')
    elif score >= 0.7:
        print('Your grade is: C')
    elif score >= 0.6:
        print('Your grade is: D')
    elif score < 0.6:
        print('Your grade is: F')
    print('\n')

Score()