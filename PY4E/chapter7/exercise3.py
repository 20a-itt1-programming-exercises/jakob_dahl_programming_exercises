file_name = input('Enter a file name: ')

if file_name == 'na na boo boo':
    print('NA NA BOO BOO TO YOU - You have been punk'd!')

else:
    try:
        file_handle = open(f'C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter7/{file_name}.txt')

        count = 0

        for line in file_handle:
            line = line.rstrip()  # remove newline character from line
            if line.startswith('Subject'):
                count = count + 1

        print(str(file_name), 'has:', str(count), 'subject lines')

    except FileNotFoundError:
        print('File cannot be opened: ', file_name)
