file_name = input('Enter a file name: ')

try:
    file_handle = open(f'C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter7/{file_name}.txt')

    confidence_total = 0.0
    count = 0

    for line in file_handle:
        line = line.rstrip()  # remove newline character from line
        if line.startswith('X-DSPAM-Confidence:'):
            confidence = (line[line.find(': ') + 1:])  # find : in line and extract everything after it
            confidence_total += float(confidence)  # add line confidence to total confidence
            count = count + 1

    print('number of entries: ', count)
    print('Average spam confidence: ', round((confidence_total/count), 12))

except FileNotFoundError:
    print('File not found')
