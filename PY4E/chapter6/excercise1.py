# Write a while loop that starts at the last character in the
# string and works its way backwards to the first character in the string,
# printing each letter on a separate line, except backwards.

userInp = str(input('> '))
userInpLen = len(userInp) - 1
while userInpLen > -1:
	print(userInp[userInpLen])
	userInpLen -=1
