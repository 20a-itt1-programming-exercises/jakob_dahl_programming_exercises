# Write an invocation that counts the number of times the letter a occurs in �banana�

count = 0

for letter in 'banana':
    if letter == 'a':
        count = count + 1

print(count)