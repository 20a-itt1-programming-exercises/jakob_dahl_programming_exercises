import random as r


comWin = 0
humWin = 0


def Main(comWin, humWin):

    randNum = r.randint(0, 9)
    attempts = 3

    if comWin == 5:
        print('Game over, the computer has won by winning 5 rounds!')
        exit(0)
    elif humWin == 5:
        print('Game over, you have won by winning 5 rounds!')
        exit(0)

    try:
        for attempt in range(attempts):
            guess = input('Enter your guess between 0 and 9 !\n> ')
            for char in guess:
                if char.isdigit():
                    guess = int(char)
                    break

            guess = int(guess)

            if guess != randNum and attempt != attempts-1:
                print('wrong guess, try again!')
            elif guess != randNum and attempt >= attempts-1:
                comWin += 1
                playAgain = input('na na I won :-P - try again? (y/n)\n> ')
                if playAgain.startswith('y'):
                    Main(comWin,humWin)
                else:
                    print('computer wins:', comWin, 'human wins:', humWin, 'see ya...')
                    break
            else:
                humWin += 1
                playAgain = input('Damn, you won!, try again? (y/n)\n> ')
                if playAgain.startswith('y'):
                    Main(comWin,humWin)
                else:
                    print('computer wins:', comWin, 'human wins:', humWin, 'see ya...')
                    break

    except ValueError:
        print('That's not even a number..... restarting')
        Main(comWin,humWin)


Main(comWin,humWin)
