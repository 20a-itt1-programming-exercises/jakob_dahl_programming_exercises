'''
Exercise 1: Write a simple program to simulate the operation of the
grep command on Unix. Ask the user to enter a regular expression and
count the number of lines that matched the regular expression:
'''

import re

txtfile = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter11/mbox.txt')
lineCount = int()

userInp = input("Enter a regular expression: ")

for line in txtfile:
    line = line.rstrip()
    if re.search(userInp,line):
        lineCount += 1

print(f"mbox.txt had {lineCount} lines that matched \"{userInp}\"")