'''
Extract the number from each of the lines using a regular expression
and the findall() method. Compute the average of the numbers and
print out the average as an integer.
'''
from statistics import mean
import re

txtfile = open('C:/Users/Jakob/Desktop/GitLab/jakob_dahl_programming_exercises/PY4E/chapter11/mbox.txt')
mboxValues = list()


for line in txtfile:
    line = line.rstrip()
    if re.search("^New .*: [0-9]+", line):
        lst = re.findall("[0-9]+", line)
        mboxValues.append(int(lst[0]))

print(mean(mboxValues))