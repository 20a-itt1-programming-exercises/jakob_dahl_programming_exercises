import sqlite3

conn = sqlite3.connect('artists.db')
cur = conn.cursor()

#album

#while True:
#    artists

while True:

    artist = str(input("Enter the name of an artist/band\n> "))

    if len(artist) > 0:

        # Make some fresh tables using executescript()
        cur.executescript('''

        CREATE TABLE IF NOT EXISTS "Artist" (
            "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, 
            "name" TEXT)
        ''')

        if len(artist) > 0:
            cur.execute(f"INSERT OR REPLACE INTO Artist (name) VALUES ('{artist}')")
        else:
            continue
    else:
        break

    conn.commit()